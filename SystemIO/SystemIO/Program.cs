﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SystemIO
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\TestDir\test.txt";

            var people = CreatPeople(20).ToList();
            //var pepole1 = people.Where(n => n.Name.StartsWith("A")).OrderBy(n => n.Name).ToList();

            using (StreamWriter write = new StreamWriter(path,false,System.Text.Encoding.Default))
            {
                foreach (var item in people)
                {
                    write.WriteLine(item);
                }
            }
            using (StreamReader read = new StreamReader(path,System.Text.Encoding.Default))
            {
                // ReadToEnd() reads from the first line to the last line
                Console.WriteLine(read.ReadToEnd());

                //ReadLine() reads row by row
                string line;
                while ((line = read.ReadLine()) != null)
                {
                        Console.WriteLine(line);                   
                }

                //read.Read(symbols, 2, 4); reads the number of characters in the given index
                ////char[] symbols = new char[10];
                ////read.Read(symbols, 2, 8);
                ////Console.WriteLine(symbols);
            }
        }

        static IEnumerable<People> CreatPeople(int count)
        {
            string[] name =
            {
                "Vahan",
                "Areg",
                "Ashot",
                "Meri",
                "Ani",
                "Elen"
            };

            string[] surename =
            {
                "Axabekyan",
                "Gevorgyan",
                "Martirosyan",
                "Hakobyan",
                "Tigranyan"
            };

            var rnd = new Random(); 
            for (int i = 0; i < count; i++)
            {
                var index = rnd.Next(0, name.Length);
                var index2 = rnd.Next(0, surename.Length);

                var people = new People(name[index], surename[index2]);

                yield return people;
            }
        }
    }

    class People
    {
        public string Name { get; set; }
        public string Surename { get; set; }

        public People(string name, string surename)
        {
            Name = name;
            Surename = surename;
        }

        public override string ToString()
        {
            return "Name: " + Name + "\tSurename:" + Surename;
        }
    }

}
